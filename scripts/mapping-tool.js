function makeMap(){

    //Create a layer containing the building shapefiles as a geoJSON
    buildingsLayer = L.geoJson(buildings, {
        style: {
                            color: '#2bdfff',

                            weight: 1,

                            fillColor: '#2bdfff',

                            fillOpacity: 0

                            }});



    //console.log(buildingsLayer);

    let osmGeo = osmtogeojson(OSMXML);

        
    //set up map
    //Create a variable containingthe map object and add the basemaps as layers
    map = L.map('map', {editable:true,
                minZoom: 15,
                        maxZoom: 22,
                        zoomControl: false

                    });

        
    //set up map
    //Create a variable containingthe map object and add the basemaps as layers
    inset = L.map('inset', {minZoom: 12,
                        maxZoom: 20,
                        zoomControl: false,
                attributionControl:false

                    });

    //Load bing and OpenStreetMap basemaps
    const bing = L.bingLayer('AgWUG-xtDgsFNlDhzIMjiuvn2sqdQ82E_ywrY3jp_mbrOiIgW9w7YJ39oJiKwteg', {
                        maxZoom: 22}).addTo(map);

    //Load bing and OpenStreetMap basemaps
    const bing2 = L.bingLayer('AgWUG-xtDgsFNlDhzIMjiuvn2sqdQ82E_ywrY3jp_mbrOiIgW9w7YJ39oJiKwteg', {
                        maxZoom: 20}).addTo(inset);  
        
        
    //Add OSM data to a GeoJSON
    osmLayer =  L.geoJson(osmGeo, {style: {
        color: '#f21b7d',

        weight: 1,

        fillColor: '#ff91c3',

        fillOpacity: 0.3

        }});

    //Ad empty geojson to display drawn buildings and set style
    insetBuildings = L.geoJSON().addTo(inset);
    drawnBuildings = L.geoJSON().addTo(map);


    drawnBuildings.setStyle({
                            color: '#f21b7d',

                            weight: 1,

                            fillColor: '#ff91c3',

                            fillOpacity: 0.3

    });	


    //Initialise empty list to store iDs
    let iDs = [];

    //Loop through building layer and see if each building intersects with the OSM layer
    //If it does add its ID to the list of IDs
    buildingsLayer.eachLayer(function(layer1){

        //Loop through OSM layer
        osmLayer.eachLayer(function(layer2){


            //building feature as a turf polygon and buffer by 1m
            let DB = turf.polygon(layer1.feature.geometry.coordinates[0]);
            let dbBuff = turf.buffer(DB, 1, {units:'meters'});

            //OSM feature as a turf polygon
            let osmGeom = turf.polygon(layer2.feature.geometry.coordinates);

            //Check if the OSM layer and building feature intersect
            let intersection = turf.booleanIntersects(osmGeom, dbBuff);

            //If they intersect push iD of proposed building feature to array
            if(intersection){

                iDs.push(layer1.feature.id);

            }

        })
    });
        

    //Remove buildings that intersect the OSM data based on IDs
    buildingsLayer.eachLayer(function(layer){

        for (i = 0; i < iDs.length; i++) { 

            if(layer.feature.id == iDs[i]){

                buildingsLayer.removeLayer(layer);

            }
        }


    });

    //Initialise empty list to store all building coordinates
    let coord = [];

    console.log(buildingsLayer.getLayers().length);
        
    //Initiliase spatial index
    tree = new RBush(buildingsLayer.getLayers().length);
        
    //Loop through buildings and get their coordinates so the original building coordinates are stored?
    buildingsLayer.eachLayer(function(layer){


        let bBounds = layer.getBounds();

        //console.log(bBounds);

        let item = {
            minX: bBounds._southWest.lng,
            minY: bBounds._southWest.lat,
            maxX: bBounds._northEast.lng,
            maxY: bBounds._northEast.lat
        };
        
        tree.insert(item);
            


    });


    //Loop through buildings and get their coordinates so the original building coordinates are stored?
    buildingsLayer.eachLayer(function(layer){


            building = layer.feature;

        //let bBounds = layer.getBounds();

    });

    buildingID = building.id;

    console.log(buildingID);


    let coords = [];

    // Loop length of the data
    for (let i = 0; i < building.geometry.coordinates[0][0].length; i++){


            coords.push([building.geometry.coordinates[0][0][i][1],building.geometry.coordinates[0][0][i][0]])

                            }							


    backUp = coords;


    //Add buildings to maps
    buildingsLayer.addTo(map);


    //Loop through buildings and get their coordinates so the original building coordinates are stored?
    buildingsLayer.eachLayer(function(layer){


            if(layer.feature.id == buildingID){

            buildingsLayer.removeLayer(layer);
            

        }




    });


    //Add OSM layer to map
    osmLayer.addTo(map);
        
    //prevent map from zooming when double-clicking 
    map.doubleClickZoom.disable();

    //Prevent users dragging the map 
    map.dragging.disable();


    activeBuilding = L.polygon(coords, {
                                    transform: true,
                                    draggable: true});


    //Create a layer containing the building shapefiles as a geoJSON
    activeBuilding.setStyle({
                            color: '#2bdfff',

                            weight: 1,

                            fillColor: '#2bdfff',

                            fillOpacity: 0

                            });


    //activeBuilding.addTo(map);

    //Set view of overview map to the entire grid
    map.fitBounds(activeBuilding.getBounds(), {maxZoom: 22});
        
    activeBuilding.addTo(map);

    activeBuilding.transform.enable({boundsOptions:{color: 'black'}, rotateHandleOptions:{color:'black'}, uniformScaling: true, dragging:true});

    activeBuilding.transform.disable();
    activeBuilding.transform.enable();

    //Enable editing on feature

    //Set map view
    //inset.setView(L.latLng(lon,lat), 18);

    // create an orange rectangle
    const gridOutline = L.rectangle([[miny, minx], [maxy,maxx]], {color: "#000000", weight: 1, fillOpacity: 0}).addTo(inset);


    //Set view of overview map to the entire grid
    inset.fitBounds(gridOutline.getBounds(), {maxZoom: 20});


    // prevent map from zooming when double-clicking
    inset.doubleClickZoom.disable();

    //disable dragging
    inset.dragging.disable();

    inset.scrollWheelZoom.disable();

    gridOutline.addTo(map);


    //Add an event listener to the green button which when clicked uploads the selected building
    document.getElementById("greenBttn").addEventListener("click", function(event){
        
        //Prevent default click behaviour
        event.preventDefault();

        //Change button to say uploading

        let button = document.getElementById("greenBttn");
        button.innerHTML = "UPLOADING...";
        button.disabled = true; 

        //Initalise tag variable			
        let tag;

        //If circle variable = true tag = hut, else = yes
        /*if(circle === true){
            tag = "hut";
        } else {
            tag = "yes";
        };*/

        //Convert geometry to a geoJSON
        bG = activeBuilding.toGeoJSON();

        //Get coordinates of the feature to be uploaded to the database
        geom = bG.geometry.coordinates[0];

        //Add to drawn buildings layer
        drawnBuildings.addData(bG);        
        insetBuildings.addData(bG);        

        //Set style of drawn buildings - do I need this???
        drawnBuildings.setStyle({
                                color: '#f21b7d',

                                weight: 1,

                                fillColor: '#ff91c3',

                                fillOpacity: 0.3

        });


        //Set style of drawn buildings - do I need this???
        insetBuildings.setStyle({
                                color: '#f21b7d',

                                weight: 2,

                                fillColor: '#ff91c3',

                                fillOpacity: 0.3

        });

        //Testing
        tag = "building"

        //map.removeLayer(activeBuilding);
        
        //nextBuilding(tree); 

        activeBuilding.transform.disable();


        upload(tag,geom)
            .then((result)=>{

                //Send ID and coordinates to database query via a POST request - send ID of the feature and its coordinates
                // may or may not be updated
                makePOSTRequest('./scripts/updateGeomNew.php', buildingID, tag, geom, usersName, function(data){


			//Add to points
                        let currentPoints = parseInt(document.getElementById('mapped').innerHTML);            
            
                        document.getElementById('mapped').innerHTML = currentPoints + 1; 

                        //Add to points
                        let totalPoints = parseInt(document.getElementById('total-points').innerHTML);            
            
                        document.getElementById('total-points').innerHTML = totalPoints + 1; 
	
			updateLeaderBoard(data);

                });

                nextBuilding();

                let button = document.getElementById("greenBttn");
                button.innerHTML = "UPLOAD BUILDING";
                button.disabled = false; 
        
            });

    });
        
    //Add an event listener to the red button which when clicked marks feature as not a building in the database and isn't uploaded to OSM
    document.getElementById("redBttn").addEventListener("click", function(evt){
        
        //Prevent default click behaviour
        evt.preventDefault();

        //Get the username of the user
        //let userName = document.getElementById("display_name").innerHTML;
        
        let button = document.getElementById("redBttn");
        button.innerHTML = "REJECTING...";
        button.disabled = true; 

        //map.removeLayer(activeBuilding);
        
        activeBuilding.transform.disable();

        //Convert geometry to a geoJSON
            bG = activeBuilding.toGeoJSON();

            //Get coordinates of the feature to be uploaded to the database
            geom = bG.geometry.coordinates[0];

        reject(geom)
                .then((result)=>{
        
                    nextBuilding();
        
                    let button = document.getElementById("redBttn");
                    button.innerHTML = "NOT A BUILDING";
                    button.disabled = false; 
        
                    makeRequest(['./scripts/markNotBuildingNew.php?id=', buildingID, '&user=', usersName].join(''), function(data){

			//Add to points
			let currentPoints = parseInt(document.getElementById('rejected').innerHTML);		
            
			document.getElementById('rejected').innerHTML = currentPoints + 1; 

                        //Add to points
                        let totalPoints = parseInt(document.getElementById('total-points').innerHTML);            
            
                        document.getElementById('total-points').innerHTML = totalPoints + 1; 

			updateLeaderBoard(data);

                })


            });


    });
        
    //Add an event listener to the grey button which when clicked marks feature as class unsure
    document.getElementById("greyBttn").addEventListener("click", function(evt){
        //Prevent default click behaviour
        evt.preventDefault();
        
        let button = document.getElementById("greyBttn");
        button.innerHTML = "WORKING...";
        button.disabled = true; 

        //map.removeLayer(activeBuilding);

        activeBuilding.transform.disable();

        //Convert geometry to a geoJSON
            bG = activeBuilding.toGeoJSON();

            //Get coordinates of the feature to be uploaded to the database
            geom = bG.geometry.coordinates[0];

            unsure(geom)
                    .then((result)=>{
        
                    nextBuilding();
        
                    let button = document.getElementById("greyBttn");
                    button.innerHTML = "NOT SURE";
                    button.disabled = false; 

 		    newLeaderboard = makeRequestPromise(['./scripts/buildingUnsureNew.php?id=', buildingID, '&user=', usersName].join(''));

		    return newLeaderboard

		}).then((result)=>{

			//Add to points
                        let currentPoints = parseInt(document.getElementById('notSure').innerHTML);            
            
                        document.getElementById('notSure').innerHTML = currentPoints + 1; 

                        //Add to points
                        let totalPoints = parseInt(document.getElementById('total-points').innerHTML);            
            
                        document.getElementById('total-points').innerHTML = totalPoints + 1; 

                        
                        updateLeaderBoard(result);

		})
        
        
    });

    //If user clicks reset button - reset the feature to the original geometry
    document.getElementById("resetButton").addEventListener("click", function(e){
                //Prevent default click behaviour
                e.preventDefault();

                resetFeature(activeBuilding, backUp);
                
        });

    //Add an event listener to the zoom to button
    document.getElementById("zoomTo").addEventListener("click", function(evt){
            evt.preventDefault();
                
            //fit map to bounds of the feature
            map.fitBounds(activeBuilding.getBounds(), {maxZoom: 20});

    });

    //Add an event listener to the hide button
    document.getElementById("Hide").addEventListener("click", function(evt){

                //Prevent default click behaviour
                evt.preventDefault();
                    
                //Enable dragging
                activeBuilding.transform.disable();
        
                //Switch to show button
                let show = document.getElementById("Show");
                show.style.display='block';

                //Remove feature from map so it isn't visible to the user
                map.removeLayer(activeBuilding);
        
                //Hide the hide button
                let hide = document.getElementById("Hide");
                hide.style.display='none';
    });

    //Add an event listener to the show building button to show building
    document.getElementById("Show").addEventListener("click", function(evt){
                    
                evt.preventDefault();

                //Switch to hide button
                let hide = document.getElementById("Hide");
                hide.style.display='block';

                //Add feature back to map so it is visible to the user
                map.addLayer(activeBuilding);
        
                //Enable path transform (scale, rotate and drag)
                activeBuilding.transform.enable({boundsOptions:{color: 'black'}, rotateHandleOptions:{color:'black'}, uniformScaling: true});

                //Hide the show button
                let show = document.getElementById("Show");
                show.style.display='none';

        });


    //Add an event listener to circularise the building
    document.getElementById("circularise").addEventListener("click", function(evt){

        evt.preventDefault();
                
        //Circularise  polygon function
        circularisePoly(activeBuilding, map);
                                                        
        //Reset transform handlers                                 
        activeBuilding.transform.reset();

        //Not sure I need this when not using non-uniform scaling???
        //Disable transform and then reenable
        activeBuilding.transform.disable;
        activeBuilding.transform.enable({boundsOptions:{color: 'black'}, rotateHandleOptions:{color:'black'}, uniformScaling: true});

    });

    //Add an event listener to the grey button which when clicked marks feature as class unsure
    document.getElementById("finished").addEventListener("click", function(evt){



		//Unlock square 
		unlockSquare(id)
			.then((result)=>{

				//Go to about page
				window.location.href = "./About.php";

				
			})



	})



}

async function upload(tag,geom){


	return "hello";

}
    
async function unsure(geom){


        return "hello";

}

async function reject(geom){


        return "hello";

}

function nextBuilding (){
    
    
    //Get feature in the spatial index
	let bBounds = activeBuilding.getBounds();
	let result = tree.search({
                minX: bBounds._southWest.lng,
                minY: bBounds._southWest.lat,
                maxX: bBounds._northEast.lng,
                maxY: bBounds._northEast.lat

        });


    if (tree._maxEntries <= 1){

	//bonus points
	bonusPoints(usersName)
		.then((results)=>{

		        location.reload();
		})


    } else {

        //get neighbour of the active building
        let neighbor = knn(tree, bBounds._southWest.lng, bBounds._southWest.lat, 2);
        let poly, nBounds; 


        if  (!neighbor[1]){

	        //bonus points
	        bonusPoints(usersName)
	                .then((results)=>{

                	        location.reload();
        	        })



	}

	else if (neighbor[0] === result[0]){

            console.log("true");
            tree.remove(result[0]);
            tree.remove(neighbor[0]);

            nBounds = [[neighbor[1].minY, neighbor[1].minX], [neighbor[1].maxY,neighbor[1].maxX]];
            poly = turf.polygon([[[neighbor[1].minX, neighbor[1].minY], [neighbor[1].maxX, neighbor[1].minY], [neighbor[1].maxX, neighbor[1].maxY], [neighbor[1].minX, neighbor[1].maxY], [neighbor[1].minX, neighbor[1].minY]]]);
            //map.fitBounds(nBounds);


        } else {
            tree.remove(result[0]);
            tree.remove(neighbor[0]);
            nBounds = [[neighbor[0].minY, neighbor[0].minX], [neighbor[0].maxY,neighbor[0].maxX]];
            poly = turf.polygon([[[neighbor[0].minX, neighbor[0].minY], [neighbor[0].maxX, neighbor[0].minY], [neighbor[0].maxX, neighbor[0].maxY], [neighbor[0].minX, neighbor[0].maxY], [neighbor[0].minX, neighbor[0].minY]]]);
            //map.fitBounds(nBounds);

            console.log("false");

        }

        //Remove active building from the spatial index
        //tree.remove(result[0]);

        

        //fit map bounds to the neighbour

        // create an orange rectangle
        //let nRect = L.rectangle(nBounds).addTo(map);

    //	console.log(activeBuilding);

        //let newRectangle; 

        buildingsLayer.eachLayer(function(layer){


            //building feature as a turf polygon and buffer by 1m
                let layeR = turf.polygon(layer.feature.geometry.coordinates[0]);

            //console.log(layeR);

                //Check if the OSM layer and building feature intersect
                let intersection = turf.booleanIntersects(poly, layeR);

            //console.log(intersection);

                //If they intersect push iD of proposed building feature to array
                if(intersection){

                newRectangle = layer;	

		buildingID = layer.feature.id; 
                console.log(buildingID);



                buildingsLayer.removeLayer(layer);
                }

//            buildingsLayer.removeLayer(layer);

        });

        let nCoords = [];

        // Loop length of the data
        for (let i = 0; i < newRectangle.feature.geometry.coordinates[0][0].length; i++){


                nCoords.push([newRectangle.feature.geometry.coordinates[0][0][i][1],newRectangle.feature.geometry.coordinates[0][0][i][0]])

                            }                                                       

        activeBuilding.setLatLngs(nCoords);

        backUp = nCoords;

//        activeBuilding = L.polygon(nCoords, {
//                                    transform: true,
//                                    draggable: true}).addTo(map);

        //Enable editing on feature
        activeBuilding.transform.enable({boundsOptions:{color: 'black'}, rotateHandleOptions:{color:'black'}, uniformScaling: true, dragging:true});    
        
        map.fitBounds(activeBuilding.getBounds());

    }


}

/**
* Function to get users points
*
*/ 

function getUsersPoints(username){

        let user = username;

        makeRequest(['./scripts/findCentaurPoints.php?user=', user].join(''), function(data){


        });     

	return data;


}

    

/**
* Make a request for JSON over HTTP, pass resulting text to callback when ready
*/
function makePOSTRequest(url, id, tag, geom, user, callback) {


// create request instance
let request = new XMLHttpRequest();

// initialise request withURL
request.open('POST', url);

// set headers (that you are sending JSON)
//request.setRequestHeader('Content-Type', 'application/json');

// set listener for when the response comes from the server
request.onreadystatechange = function () {

// check the response is OK
if (this.readyState === 4) {

    // just print out some information
    console.log('Status:', this.status);
    console.log('Headers:', this.getAllResponseHeaders());
    console.log('Body:', this.responseText);

    callback(JSON.parse(this.responseText));

}
};


// set the data you want to pass to the server as a JSON String
const body = '{"id":' + id.toString() + ',"tag": "' + tag.toString() + '"' + ',"user": "' + user.toString() + '"' + ',"geom": {"type":"MultiPolygon","coordinates":[[' + JSON.stringify(geom) + ']]}' + '}';


// send the request
request.send(body);

}
    
    
/**
 * Make a request for JSON over HTTP, pass resulting text to callback when ready
 */
function makeRequest(url, callback) {

    //initialise the XMLHttpRequest object
    var httpRequest = new XMLHttpRequest();

    //set an event listener for when the HTTP state changes
    httpRequest.onreadystatechange = function () {

        //a successful HTTP request returns a state of DONE and a status of 200
        if (httpRequest.readyState === XMLHttpRequest.DONE && httpRequest.status === 200) {
                //Parse returned json

                callback(JSON.parse(httpRequest.responseText));
        }
    };

    //prepare and send the request
    httpRequest.open('GET', url);
    httpRequest.send();
}


/**
* FUNCTION: reset button
*/		

function resetFeature (layer, orig){

        layer.setLatLngs(orig);
        layer.transform.reset();
        layer.transform.disable;
        layer.transform.enable({boundsOptions:{color: 'black'}, rotateHandleOptions:{color:'black'}, uniformScaling: true});

}

/**
* Function to circularise  polygons once option selected from menu
*
*/ 
    
function circularisePoly(building, map){

        circle = true;

        //get original coordinates
        orig  = building.toGeoJSON();

        //get centroid of the right clicked feature
        let centroid = turf.centroid(orig);


        //set up an array to store the distances between the centroid and the vertices
        let distances = [];

        
        // Loop length of the data
        for (let i = 0; i < orig.geometry.coordinates[0].length; i++){
                

                //Get coordinate pair of the vertex
                let coordinatePair = turf.point(orig.geometry.coordinates[0][i]);

                //work out distance between vertex and the centroid
                let distance = turf.distance(centroid, coordinatePair, {units: 'kilometres'});

                //store in the distance array
                distances.push(distance)

        }

        //find out max distance and store as a variable
        let maxDist = Math.max.apply(null, distances);

        let rad = maxDist * 1000

        // Multiply max distance by sqrt of 2
        let buffDist = maxDist / Math.sqrt(2);

        //buffer from centroid.
        let buffered = turf.buffer(centroid, buffDist, {units: 'kilometres'});

        //Get new coordinates of the feature - in leaflet style LatLong
        newCoords = [];


        for (let i = 0; i < buffered.geometry.coordinates[0].length; i++){
            newCoords.push([buffered.geometry.coordinates[0][i][1], buffered.geometry.coordinates[0][i][0]]);
            //console.log(buffered.geometry.coordinates[0][i][0]);
        };

        //Update coordinates of polygon layer
        building.setLatLngs(newCoords);

        return circle;

}

async function getUserDetails (username) {

    return username;
}

/* Make a request for JSON over HTTP, pass resulting text to callback when ready
* Returns a promise
*/
function makeRequestPromise(url) {

		return new Promise(function (resolve, reject) {

                	//initialise the XMLHttpRequest object
                	var httpRequest = new XMLHttpRequest();

                	//set an event listener for when the HTTP state changes
                	httpRequest.onreadystatechange = function () {
                
					//a successful HTTP request returns a state of DONE and a status of 200
					if (httpRequest.readyState === XMLHttpRequest.DONE && httpRequest.status === 200) {
                                        
					//Parse returned json
					resolve(JSON.parse(httpRequest.responseText));
                        	}
                	};

                	//prepare and send the request
                	httpRequest.open('GET', url);
                	httpRequest.send();

		})
}

function updateLeaderBoard (scores){

	console.log(scores);

 	let sortedScores = scores.sort(compare);

	document.getElementById('user-first').innerHTML = sortedScores[0][0];
	document.getElementById('points-first').innerHTML = sortedScores[0][1];
	document.getElementById('user-second').innerHTML = sortedScores[1][0];
	document.getElementById('points-second').innerHTML = sortedScores[1][1];
	document.getElementById('user-third').innerHTML = sortedScores[2][0];
	document.getElementById('points-third').innerHTML = sortedScores[2][1];
	document.getElementById('user-fourth').innerHTML = sortedScores[3][0];
	document.getElementById('points-fourth').innerHTML = sortedScores[3][1];
	document.getElementById('user-fifth').innerHTML = sortedScores[4][0];
	document.getElementById('points-fifth').innerHTML = sortedScores[4][1];



}

function compare(a,b){return b[1] - a[1]}  


async function bonusPoints(user) {

	makeRequest(['./scripts/bonusPoints.php?user=', user].join(''), function(data){});

}

async function unlockSquare(id) {

        makeRequest(['./scripts/unlockSquare.php?id=', id].join(''), function(data){});

}
