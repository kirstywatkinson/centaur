<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">  		
	        <title>Centaur VGI</title>

        	<link rel="icon" type="image/png" href="./images/favicon.png"/>

		<link rel="shortcut icon" href="#">
                <!--load osm-auth -->
                <script src="./scripts/osm-auth-main/osmauth.js"></script>
                <!-- load js require -->
                <script src="./node_modules/npm-require/src/npm-require.js"></script>
                <!-- Load CSS for Leaflet -->
                <!--<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />-->
                <link rel="stylesheet" href="./scripts/leaflet/leaflet.css" />
                <!-- Load JavaScript for Leaflet -->
                <!--<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>-->
                <script src="./scripts/leaflet/leaflet.js"></script>
            
                <!-- Load CSS for Context Menu-->
                <link rel="stylesheet" href="./scripts/leaflet.contextmenu.css"/>
                <!-- Load Javascript for Turf -->
                <script src="./scripts/turf.min.js"></script>
                <!-- Load Javascript for Bing tiles-->
		<script src="./scripts/Bing.js"></script>
                <script src="./scripts/Bing.addon.applyMaxNativeZoom.js"></script>
                <!-- Polyfill-->
                <!--<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise"></script>-->
        
		<style>
            body, form,  html{
                        width: 100%;
                        height: 100%;
                        margin: 0;
                }

            /* The map */
            #map {
                        width: 100%;
                        height: 100%;
                        z-index: 0;
                }
        
            
        ul {
              list-style-type: none;
              margin: 0;
              padding: 0;
              overflow: hidden;
	      z-index: 500;
            }

            li {
              float: left;
		z-index: 500;
            }

            li a {
              display: block;
              color: white;
              text-align: center;
              padding: 18px 16px 0px 18px;
              text-decoration: none;
              font-family: 'Arial', sans-serif;
                font-weight: 400;
            }
                        
			
		/* style the container */
		/*#container {
			width: 100%;
			height: 100%;

		}*/
		
		/* style the header text */
		#header-text {
			font-family: 'Arial', sans-serif;
            		font-weight: 700;
			text-align: center;
			font-weight: bold;
			font-size: 72px;
			position: relative;
			color: white;
			<!-- left: 100px; -->
			<!-- top: 17px; -->
		}
		
				
		/* style the header text */
		#paragraph-text {
			top: 15%;
			color: white;
			font-family: 'Arial', sans-serif;
			position: relative;
			padding: 0px 30px 10px 30px;
			text-align: center;
			z-index: 500;
			width:60%;
			left: 20%;
		}
		
		.wrapper{
			width: 100%;
			text-align: right;
						
		}
		
		
		p {
			font-size: 24px;
			color: white;

		}

		p2 {
			font-size: 30px;

		}

		#paragraph-a {
			color: white;

		}
		#paragraph-a:visited {

			color: white;

		}

		/* style the container */
		#button-container {
			text-align: center;
			width: 100%;
		}


		   /* style the container */
                #button-container2 {
                        text-align: center;
                        width: 100%;
                }




		/* style the container */
		#button-container {
			text-align: center;
			width: 100%;
			z-index: 500;
		}
	
		/* style the container */
                #login-container {
                        text-align: center;
                        width: 100%;
                }
		

		
		#header-container{
		
			width: 60%;
			left: 20%;
			text-align: center;
			position: relative;
			z-index: 500;
			
		}

		#unauthenticated{
			width: 70%;
                        left: 15%;
                        text-align: center;
                        position: relative;
                        z-index: 500;

                }





		#ul-container{
			width: 100%;
                        text-align: right;
                        position: relative;
                        z-index: 500;



		}

		
		.button2{
			margin-top: 15px;
			padding-right: 5px;
                        padding-top: 5px;
                        padding-bottom: 5px;
                        padding-left: 5px;
                        background-color: white;
                        border: black 2px solid;
                        cursor: pointer;
                        font-family: 'Arial', sans-serif;
                        font-weight: bold;
                        font-size: 16px;
                        width: auto;
                        color: black;
			z-index: 500;
                }

        
		.button2:hover{
			border: black 3px solid;

		}

		/* Free text answer space */
            
		.textarea {
      			width: calc(100% - 6px);
      			font-family: Arial;
			font-size: 16px;
			color: #666;

		}



		table, th {
 		 	border: 1px solid black;
			font-family: Arial;
                        font-weight: bold;
                        font-size: 16px;	
			background-color: white;
			color: black;
			text-align: center;
		}

		td {
			font-weight: normal;
			border: 1px solid black;
		}



		@viewport {
                        width: device-width ;
                        zoom: 1.0 ;
                }

		@media screen and (max-width: 1366px) and (min-width: 1100px){

		p {

			font-size: 20px;
		}

		.button {
                        width: 7%;
                        height: 100%;
                }

                .buttone {
                        width: 18%;
                        height: 80%;

                }

                .buttonh {
                        width: 15%;
                        height: 75%;
                }

		  .buttonl {
                        width: 15%;
                        height: 75%;
                }

                .buttonr {
                        width: 15%;
                        height: 75%;
                }


		
		#button-container {
			width: 70%;

			left: 15%;
		}

		#header-container{
                        width: 70%;
                        left: 15%;

                }

                 #paragraph-text{
                        width: 70%;
                        left: 15%;

                }


		#header-text {
			font-size: 65px;

		}

		}

		@media screen and (max-width: 1099px) and (min-width: 600px){
		
		p {

                        font-size: 20px;
                }

		.button2{

			font-size: 13px;
		}

                #button-container {
                        width: 90%;

			left: 5%;
                }

		#header-container{
			width: 80%;
			left: 10%;

		}
			
		 #paragraph-text{
                        width: 80%;
                        left: 10%;

                }



                #header-text {
                        font-size: 50px;

                }


		}


	
		</style>
	</head>
	
	<body>
		
            <div id="map">
			
            <!-- header -->
						
			<div id='header-container'>
				<div id='header-text' style="margin-top:10%">
					<div id='unauthenticated'>
		            				<p style="font-weight: normal">The tool you are about to use is called Centaur VGI and combines a computer and a volunteer (you) to map buildings in the Acholi region of Northern Uganda. Your task is to approve, edit, or reject objects predicted by a computer as a building using machine learning. Approved buildings will be uploaded to OpenStreetMap and used to inform on-the-ground mapping campaigns to help create up-to-date maps of the region, which will be used to support the work of the Gulu Referral hospital. </p>
		
							<!--<form id="name" action='./scripts/insertUser.php' method="post">-->
								<p style="font-weight: normal"> To begin either log in with your existing username or sign up: </p>
									<p id="noUserMessage" style="font-weight:normal; display:none">Sorry no username exists, please sign up:</p>	


								<button class="button2" id="logIn">Log in</button> <button class="button2" id="signUp">Sign up</button>

								 <div id="signUp2" style="display:none">
								
									<p id="userNameError" style="font-weight:normal; display:none">Sorry username already exists, please select a different username</p>	


									<input type="text" id="username" placeholder="Enter Username" class="textarea">

									<button class="button2" id="newUser">Enter username</button>

								</div>

								<div id="logIn2" style="display:none">
        
									<input type="text" id="usernameLogIn" placeholder="Enter Username" class="textarea">
        
	                                                                <button class="button2" id="existingUser">Enter username</button>

                                                                </div>
								<div id="startMapping" style="display:none">

									<button class="button2" onclick="window.location.href='./mapping-tool.php'">Start Mapping</button>

								</div>


							<!--</form>	-->

					</div>
					
            
				</div>
			</div>
                

		</div>

<script>

		// document.getElementById('link').innerHTML = "<a href='" + getNextPath() + "'>Next</a>"; 

		//console.log(getNextPath());


		let userName, userValue;

		var OsmRequest = require('osm-request');

		//Create osm-request
		//main osm api
       	const osm = new OsmRequest({
  		endpoint: 'https://www.openstreetmap.org',
  		oauthConsumerKey: 'Na9yRlHHno5HaMhQUg4B58nscYrBdA7uiyvV52mq',
  		oauthSecret: '27cJAoPleuQQxaD6svDcpis5LC7Tg4Lc0HvQ2Yl5',
		auto: true});
		
	/*	const osm = new OsmRequest({
                        endpoint: 'https://www.openstreetmap.org',
                        oauthConsumerKey: 'G0XsFpRQsvciKAkHLyBSSpD04ykte-IQ3AJWKxpPnIU',
                        oauthSecret: 't0IGSDtdeUVLMPYIbxJVUizKzQkM1eLM5F0g_PazUgU',
                        auto: true});*/

	if (!osm._auth.bringPopupWindowToFront()) {
                                osm._auth.authenticate(function() {
                                        //Update function
                                });

	}


                
        //Bounds of Gulu district
        min_x = 32.12395;
        max_x = 32.82805;
        min_y = 2.4515;
        max_y = 3.3066;

        //Generate random coordinates within Gulu district
        let lon = min_y + (Math.random() * (max_y - min_y));
        let lat = min_x + (Math.random() * (max_x - min_x));
        

        //set up map
        //Create a variable containingthe map object and add the basemaps as layers
        map = L.map('map', {minZoom: 15,
                            maxZoom: 18,
                            zoomControl: false
                        });
            
        //Load bing and OpenStreetMap basemaps
        const bing = L.bingLayer('AgWUG-xtDgsFNlDhzIMjiuvn2sqdQ82E_ywrY3jp_mbrOiIgW9w7YJ39oJiKwteg', {
                               maxZoom: 18}).addTo(map);
                                        
        //Set map view
        map.setView(L.latLng(lon,lat), 18);
                 
        // prevent map from zooming when double-clicking 
        map.doubleClickZoom.disable();

        //disable dragging
        map.dragging.disable();      

	map.scrollWheelZoom.disable();  
            

	document.getElementById('logIn').addEventListener("click", function(evt){

               document.getElementById('signUp').style.display = "none";

		document.getElementById('logIn').style.display = "none";

		document.getElementById('logIn2').style.display = "block"

	});

	document.getElementById('signUp').addEventListener("click", function(evt){

		document.getElementById('signUp').style.display = "none";
                document.getElementById('logIn').style.display = "none";

		document.getElementById('signUp2').style.display = "block"

	});



	document.getElementById('newUser').addEventListener("click", function(evt){
                
                	let user = document.getElementById('username').value;

                	makeRequest(['./scripts/insertUser.php?user=', document.getElementById('username').value].join(''), function(data){

                                console.log(data)

			document.getElementById('noUserMessage').style.display = "none";			




			if(data == 'Exists'){

				console.log("Sorry username not unique");

                                document.getElementById('userNameError').style.display = "block";



				//Error message sorry that username already exists

			} else {

				document.getElementById('signUp2').style.display = "none";

				document.getElementById('startMapping').style.display = "block";

				console.log("Username accepted");

				//Say welcome switch button to start mapping

				localStorage.setItem("username", user);

			}

	                });

	})


        document.getElementById('existingUser').addEventListener("click", function(evt){


			let user = document.getElementById('usernameLogIn').value;

			makeRequest(['./scripts/checkUser.php?user=', document.getElementById('usernameLogIn').value].join(''), function(data){

				if(data == 'NoUser'){

					document.getElementById('logIn2').style.display = "none";

					document.getElementById('noUserMessage').style.display = "block";
			                document.getElementById('signUp2').style.display = "block";

				} else {

		                       document.getElementById('logIn2').style.display = "none";

                		        document.getElementById('startMapping').style.display = "block";

                        		localStorage.setItem("username", user);

				}

		});

	});


/**
 * Make a request for JSON over HTTP, pass resulting text to callback when ready
 */
function makeRequest(url, callback) {

    //initialise the XMLHttpRequest object
    var httpRequest = new XMLHttpRequest();

    //set an event listener for when the HTTP state changes
    httpRequest.onreadystatechange = function () {

        //a successful HTTP request returns a state of DONE and a status of 200
        if (httpRequest.readyState === XMLHttpRequest.DONE && httpRequest.status === 200) {
                //Parse returned json

                callback(JSON.parse(httpRequest.responseText));
        }
    };

    //prepare and send the request
    httpRequest.open('GET', url);
    httpRequest.send();
}

        
		</script>
	</body>

</html>
