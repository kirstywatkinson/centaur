<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />-->

<script src="./scripts/leaflet/leaflet.js"></script>
<link rel="stylesheet" href="./scripts/leaflet/leaflet.css"/>

<!-- Load JavaScript for Leaflet -->
<!--<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>-->

<!-- Load Javascript for Bing tiles-->
<script src="./scripts/Bing.js"></script>
<script src="./scripts/Bing.addon.applyMaxNativeZoom.js"></script>
    
    
<!-- Load Javascript for Leaflet Path Transform -->
<script src="./scripts/Leaflet.Path.Transform/dist/L.Path.Transform_v3.js"></script>

<!--<script src="./scripts/Path.drag.js"></script>-->
<!-- Load Javascript for Leaflet Editable -->
<script src="./scripts/Leaflet.Editable.js"></script>

<!--load osm-auth -->
<!-- load js require -->
<script src="./node_modules/npm-require/src/npm-require.js"></script>
<!-- Polyfill-->
<script src="./scripts/osmtogeojson/osmtogeojson.js"></script>
    
<!-- R-bush for spatial indexing -->
<!--<script src="https://unpkg.com/rbush@2.0.1/rbush.min.js"></script>-->
<!--<script src="https://unpkg.com/rbush-knn"></script>-->

<!-- Load Javascript for Turf -->
<!--<script src="https://cdn.jsdelivr.net/npm/@turf/turf@6.3.0/turf.min.js"></script>-->
<script src="./scripts/turf.min.js"></script>

<script src="./scripts/mapping-tool.js"></script>


<style>
body, html{

	margin: 0;
	width: 100%;
	height: 100%;
}

* {

 box-sizing: border-box;
}

/* Create three equal columns that floats next to each other */
.column {
  float: left;
  width: 80%;
  height: 55%;
  background-color: #ffffff;

}
    
#map {
            width: 80%;
            height: 90%;
            z-index: 0;
            border-top: 2px solid black;
            border-right: 2px solid black;
           }


.columnside {

  float: right;
  width: 20%;
  border-left: 10px solid #ffffff;
  height: 100%;
  z-index: 0;
}


.column2 {

float: left;
  width: 100%;
  height: 100%;
	/*background-image: linear-gradient(rgba(247, 249, 251, 0.8), rgba(247, 249, 251, 0.8)), url(./Images/tile_tsp.png);*/
  background-position: center;
  background-size: 500px 500px;

}

.column3 {

float: left;
  width: 100%;
  height: 100%;
  border-top: 10px solid #ffffff;
  background-color: #ffffff;
}

.columnside2 {

  float: left;
  width: 25%;
  background-color: #8db48e;
  border-left: 10px solid #ffffff;
  height: 100%;

}


.row{

	height: 100%;
}
header{
	background-color: #ffffff;
	height: 10%;
	width: 80%;
}

footer{

	height: 5%;
	width: 80%;
}
/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}


h2{
  font-family: 'Cabin', sans-serif;
  text-align: center;
  /*color: #93B292;*/
  color:black;
  font-size: 40px;

  /*text-shadow: -1px -1px 0 #000000, 1px -1px 0 #000000, -1px 1px 0 #000000, 1px 1px 0 #000000;*/
}


h3 {

	text-align: center; 
	color: #ffffff;
	font-family: 'Cabin', sans-serif;
	font-size: 40px;
}

p {


}


p2 {

	font-family: 'Cabin', sans-serif;
	color:  #000000;
	line-height: 1.5;
	z-index: 5000;
	 position: relative;
	font-size: 12px;
	/*text-shadow: 2px 2px 5px black, 2px 2px 5px black;*/
}

p3 {
        font-family: 'Cabin', sans-serif;
	font-weight: normal;
	color: #000000;
	line-height: 1.5;
	z-index: 5000;
	position: relative;
	font-size: 12px;
	/*text-shadow: 2px 2px 5px black, 2px 2px 5px black;*/
}

table, th {
			margin-left: 5%;
            border: 1px solid black;
            font-family: 'Cabin', sans-serif;
            font-weight: bold;
            font-size: 16px;
            color: black;
            text-align: center;
			padding: 5px;
			border-collapse:collapse;
                }

td {
        font-weight: normal;
        border: 1px solid black;
        padding: 5px;
}

tr {

	line-height: 20px;
}

ul {
              list-style-type: none;
              margin: 0;
              padding: 0;
              overflow: hidden;
	      display:inline-block;


                }
    
.header-link{
                display: block;
                  color: black;
                  text-align: center;
                  margin-top: 15px;
                  margin-left: 10px;
                  margin-right: 20px;
                  text-decoration: none;
                  font-family: 'Arial', sans-serif;
                  font-weight: 600;
                  font-size: 30pt;
            }

.header-link:hover{
        color: black;
            }
    
li {
  float: left;
}

li a {
  display: block;
  color: black;
  text-align: center;
  margin-top: 25px;
  margin-left: 5px;
  text-decoration: none;
  font-family: 'Arial', sans-serif;
  font-weight: 400;
  font-size: 19px;
}
    
 li a:hover{
                        color: #99BBD0;
                }


li h {
                display: block;
                color: black;
                text-align: center;
                margin-top: 10px;
                margin-left: 10px;
                margin-right: 16px;
                text-decoration: none;
                font-family: 'Arial', sans-serif;
                font-weight: 600;
                font-size: 34pt;
                
            }
/* Username */      
li h1 {
            padding-right: 5px;
            padding-top: 3px;
            padding-left: 7px;
            padding-bottom: 1px;
            background-color: #99BBD0;
            border: black 2px solid;
            cursor: pointer;
            font-family: 'Arial', sans-serif;
            font-weight: 400;
            font-size: 16px;
    }

li  img {
    display: block;
    color: black;
    text-align: center;
    margin-left: 10px;
    margin-bottom: 5px;
    text-decoration: none;

}

li p {
    font-family: 'Arial', sans-serif;
    font-weight: 400;
    font-size: 18px;
    background-color: #8FC1E3;
    text-align: center;
    border: 2px solid black;
    padding-right: 3px;
    padding-left: 3px;
    padding-top: 1px;
    padding-bottom: 1px;

}
    
    
 .link {
        display: block;
        text-align: center;
        margin-top: 5px;
        margin-left: 5px;
            }


/* I'm Finished button */

.finishedBttn{

        padding-right: 3px;
        padding-top: 3px;
        padding-bottom: 3px;
        padding-left: 3px;
        background-color: #8fe3db;
        border: black 2px solid;
        cursor: pointer;
        font-family: 'Arial', sans-serif;
        font-weight: bold;
        font-size: 18px;
        margin-top: 0px;
}

.finishedBttn:hover{
    color: rgba(255, 255, 255, 1);
    /*box-shadow: 0 5px 10px rgba(0, 0, 0, .7);*/

    }
    
    
.finish-b {

        position: relative;
        float: right;
        padding-left:7px;
        padding-top:20px;
        padding-right:5px
        }

.scores {

		color:#ffffff;
		margin-right:2%;
		font-family: 'Cabin', sans-serif; 
		font-size:70px;
		text-align: center;
		text-shadow: -1px -1px 0 #999999, 1px -1px 0 #999999, -1px 1px 0 #999999, 1px 1px 0 #999999;
	}

.img2 {

	height:140px;
	width: 140px;
	

	}
    
/**
* Approve, edit, delete and unsure buttons
*/

/** Box template */
.box {
  float:left;
  position: relative;
  width: 28.33%; /* three boxes (use 25% for four, and 50% for two, etc) */
  padding: 10px; /*if you want space between the images */
  height: 5%;
  margin-top: 1%;;
  margin-bottom: 10px;
  margin-left: 2.5%;
  margin-right: 2.5%;
  text-align: center;
  font-size: 14px;
  cursor: pointer;
  font-weight: 600;
  font-family: 'Arial', sans-serif;
  background-color: #4CAF50;
  border: 2px solid black;
  color: white;
  z-index: 1500;
  ;
}

.box2 {background-color: #008CBA;} /* Blue box */
.box3 {background-color: #f44336;} /* Red box */
.box4 {background-color: #8f8f8f;border-} /* Grey box */   

.box:hover{
            color: rgba(0, 0, 0, 1);
            /**box-shadow: 0 5px 15px rgba(0, 0, 0, .7);*/
    }

/* Container for the buttons */

.clearfix {
  content: "";
  clear: both;
  display: table;
  margin: 0;
  z-index: 1500;
  width:100%;             
  text-align: center;
}


/* Editing buttons */

/* Reset Button */  

#resetButton {

z-index: 500;
position: absolute;
top: 82%;
right: 2%;
padding: 5px;
z-index: 500;
background-color: white;
border: white 1px solid;
border-radius: 5px; !important;
cursor: pointer;
font-family: Calibri, sans-serif;
font-stretch: condensed;
text-align: center;
color: #474545;
box-shadow: 0 0 2px rgb(0,0,0,0.5);
font-size: 16px;
width: 150px;
word-wrap: break-word;
white-space: normal;                        
}

.resetButton:hover{
font-weight: bold;
}

/* Zoom To Building Button */

#zoomTo {

position: absolute;
top: 74%;
right: 2%;
width: 150px;
padding: 5px;
z-index: 500;
background-color: white;
border-radius: 5px; !important;
border: white 1px solid;
cursor: pointer;
text-align: center;
font-family: Calibri, sans-serif;
font-stretch: condensed;
font-size: 16px;
color: #474545;
text-decoration: none;
box-shadow:0 0 2px rgb(0,0,0,0.5);
word-wrap:break-word;
white-space: normal;
}
.zoomTo:hover{
    font-weight: bold;
}
/* Show/Hide Button */

.showHide {

    position: absolute;
    top: 65%;
    right: 2%;
    width: 150px;
    height: 40px;
    padding: 4px;
    z-index: 500;
    background-color: white;
    border-radius: 5px; !important;
    border: white 1px solid;
    cursor: pointer;
    text-align: center;
    font-family: Calibri, sans-serif;
    font-stretch: condensed;
    font-size: 16px;
    color: #474545;
    text-decoration: none;
    box-shadow:0 0 2px rgb(0,0,0,0.5);
    word-wrap:break-word;
    white-space: normal;
}

.showHide:hover{
    font-weight: bold;
    /*background-color: #8fe3db;*/        
}

.circularise {

    position: absolute;
    top: 90%;
    right: 2%;
    width: 150px;
    height: 30px;
    padding: 4px;
    z-index: 500;
    background-color: white;
    border-radius: 5px; !important;
    border: white 1px solid;
    cursor: pointer;
    text-align: center;
    font-family: Calibri, sans-serif;
    font-stretch: condensed;
    font-size: 16px;
    color: #474545;
    text-decoration: none;
    box-shadow:0 0 2px rgb(0,0,0,0.5);23
    word-wrap:break-word;
    white-space: normal;
}

.circularise:hover{
                        font-weight: bold;
                        /*background-color: #8fe3db;*/        
                }


#inset {
    			width: 250px;
    			height: 200px;
    			<!--position:absolute;
    			top:5%;
    			left:5%;-->
    			margin-left:-10px;
    			margin-top:-10px;
    			outline:1px solid black;
    			pointer-events:none;
		}
    

@viewport {
                        width: device-width ;
                        zoom: 1.0 ;
                }


                @media screen and (max-width: 1500px) and (min-width: 1100px) {
			h2{
				font-size: 25px;
			}

			 h3{
                                font-size: 25px;
                        }

			.scores {
				font-size: 45px;

			}

			p2 {
				font-size: 20px;

			}

			table, th {
				font-size: 18px;
			}


		}




</style>
</head>
<body>

<header>
<div class="column2" style="">
<div style="text-align:left">
    <ul><li><a href="../index.html" class="link"><img class="logo" src="./images/logo.png" height="60" width="60"></img></a></li>
			<!--<li><a href="../index.html" class="header-link">CENTAUR VGI</a></li>-->        
			<li><a href="../index.html" class="header-link">COMMUNITY MAPPING</a></li>        
			<li><a href="./Instructions.html" target="_blank" style="margin-right:10px;float:right">Instructions</a></li>
            <li><a href="./tagging_guidance.html" target="_blank">Building gallery</a></li>
    </ul>
     
                <!--<li><a href="./home.php">HOME</a></li>
                <li><a href="./about.php" target="_blank">ABOUT</a></li>-->
                
</div>

<div style="margin-top: 2%;text-align:center">
</div>
</div>
</header>
    
 <div class="column" style="" id="map">   
                        <!-- approve, reject or unsure buttons -->
                <div id="clearfix" style="width:100%;">
                        <button class="box" id="greenBttn">UPLOAD BUILDING</button>
                        <button class="box box2" id="greyBttn">NOT SURE</button>
                        <button class="box box3" id="redBttn">NOT A BUILDING</button>
                </div>  
    <div>
            <button class="resetButton" id="resetButton">Reset Building</button>
			<button class="zoomTo" id="zoomTo">Zoom to Building</button>
			<button class="showHide" id="Hide">Hide Building</button>
			<button class="showHide" id="Show" style="display: none;">Show Building</button>
			<button class="circularise" id="circularise"><img src="./images/circle.png" width="15" height="15" style="position: absolute; top:6px; left: 18px;"></img><span style="position: absolute; top: 7px; left: 38px;">Make Circle</span></button>

    </div>

</div>

<!-- color: #93B292; text-shadow: -1px -1px 0 #000000, 1px -1px 0 #000000, -1px 1px 0 #000000, 1px 1px 0 #000000 -->

<div id='columnside' class="columnside" style="top:0;position:absolute;right:0;">
<div>
    <ul style="float:right">
            <li><a href="../index.html" target="_blank" style="margin-right:10px;float:right">Home</a></li>
			<li class="finish-b"><button class="finishedBttn" id="finished">I'm Finished!</button></li>

    </ul>
</div>
<div style="">
<div style="top:10%; height:30%; position:absolute">
<table style="">
                        <tr>
                        <th> Position</th>
                        <th> Username</th>
                        <th> Points</h>
                        </tr>
                        <tr>
                        <td>1st</td>
                        <td><span id="user-first"></span></td>
                        <td><span id="points-first"></span></td>
                        </tr>
                        <tr>
                        <td>2nd</td>
                        <td><span id="user-second"></span></td>
                        <td><span id="points-second"></span></td>
                        </tr>
                        <tr>
                        <td>3rd</td>
                        <td><span id="user-third"></span></td>
                        <td><span id="points-third"></span></td>
                        </tr>
                        <tr>
                        <td>4th</td>
                        <td><span id="user-fourth"></span></td>
                        <td><span id="points-fourth"></span></td>
                        </tr>
                        <tr>
                        <td>5th</td>
                        <td><span id="user-fifth"></span></td>
                        <td><span id="points-fifth"></span></td>
                        </tr>
                        </table>
    </div>

<div style="position: absolute;top: 40%; text-align: center; height:30%; width:100%>">



   <p2>USERNAME: <span class="p3" id="user-name"> Insert text here</span></p2><br>
    <p2>POINTS TOTAL: <span class="p3" id="total-points">  </span> </p2><br>
    <p2>BUILDINGS MAPPED: <span class="p3" id="mapped">  </span>  </p2><br>
    <p2>FEATURES REJECTED: <span class="p3" id="rejected">  </span></p2><br>
    <p2>UNSURE: <span class="p3" id="notSure">  </span></p2><br>

</div>

<div style="text-align: center; position: absolute;height: 30%; top: 70%">

<div id="inset">

</div>	


		</div>
</div>

  
</div>

</body>

<?php

//connect to database
        require('./scripts/connection.php');
                

		//Get grid square
		$query = "SELECT id, ST_XMin(b), ST_YMin(b), ST_XMax(b), ST_YMax(b), concat('https://www.openstreetmap.org/edit#map=18/', ST_y(a.c), '/', ST_x(a.c)),  distance from (select id, Box2D(ST_Transform(ST_Envelope(wkb_geometry), 4326)) as b, ST_AsText(ST_Transform(ST_Centroid(wkb_geometry), 4326)) as c, distance from grid_acholi WHERE status = 0 order by distance limit 1) a";
		$result = pg_query($query);

        //Intialise empty geometry variable
		$geom = '';

        //Get results
		while ($row = pg_fetch_row($result)){

            //Lock square from other users
	    $lock = pg_query("UPDATE grid_acholi SET status = 1 WHERE id = $row[0]");
			
            //Get geometry
			$query2 = "SELECT wkb_geometry FROM grid_acholi WHERE id = $row[0] ";
			$geom = pg_fetch_row(pg_query($query2))[0];

            //Get centre of square
			$centroid = pg_fetch_row(pg_query("SELECT ST_AsGeoJSON(ST_Transform(ST_Centroid(wkb_geometry), 4326)) FROM grid_acholi WHERE id = $row[0]"))[0];

            //Construct string to query overpass api
			$str2 = 'http://overpass-api.de/api/interpreter?data=[out:json][timeout:25];(way[%22building%22](' . $row[2] . ',' . $row[1] . ',' . $row[4] . ',' . $row[3] . ');relation[%22building%22](' . $row[2] . ',' . $row[1] . ',' . $row[4] . ',' . $row[3] . '););out%20body;%3E;out%20skel%20qt;';
			
			// create curl resource
            $ch = curl_init();

			// set url
            curl_setopt($ch, CURLOPT_URL, $str2);

            //return the transfer as a string
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			// $output contains the output string
            $OSM = curl_exec($ch);

            // close curl resource to free up system resources
            curl_close($ch);

            //Echo properties of the grid to main script
			echo "<script>\n";
            echo "  var id = " . $row[0] . ";\n";
            echo "  var minx = " . $row[1] . ";\n";
            echo "  var miny = " . $row[2] . ";\n";
            echo "  var maxx = " . $row[3] . ";\n";
            echo "  var maxy = " . $row[4] . ";\n";
            echo " var centroid = $centroid \n";
            
            //Echo OSM data 
            try {
					echo "var OSMXML = $OSM \n";    
				}
            catch (exception $e) {
                
				    //code to handle the exception that there is no data
					echo "var OSMXML = '' \n";
				}

            echo "</script>\n";


		}


        //Get buildings within the square
		$queryB = "SELECT gid, ST_AsGeoJSON(ST_Transform(geom,4326)) FROM buildings WHERE ST_Intersects(ST_Transform(geom, 4326), ST_Transform('$geom', 4326)) AND status = 0";
		$resultB = pg_query($queryB);

        //If results construct geometry 
		if (pg_num_rows($resultB) > 0){
                        $output    = '';
                        $rowOutput = '';

                        while ($row = pg_fetch_row($resultB)) {
                                $rowOutput = (strlen($rowOutput) > 0 ? ',' : '') . '{"type": "Feature", "id": ' . $row[0] . ', "properties": {}, "geometry": ' . $row[1] . '}';
                                $output .= $rowOutput;
                        }

                        $output = '{"type": "FeatureCollection","name": "buildings", "crs":{"type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } }, "features": [ ' . $output . ' ]}';


                } else {

                         $output = json_encode('no buildings');

                }

	
				echo "<script>\n";
 		               	echo "let buildings = $output\n";
                		//echo "let mapped  = $output\n";
                		//echo "let notSure = $output\n";
                		//echo "let rejected = $output\n";


				echo "</script>\n";

?>

<script>
let backUp, activeBuilding, building, buildingsLayer, circle;
let bG, geom, map, tree, newRectangle, usersName, buildingID;

// as a CommonJS module
const RBush = require('rbush');
const knn = require('rbush-knn');
    
//Check if no buildings - if not reload map 
if(buildings == 'no buildings'){

    //Switch to a different square
    location.reload();


} else {

    usersName = localStorage.getItem("username");

    document.getElementById("user-name").innerHTML = usersName;

    getUserDetails(usersName)
        .then((result) => {

            //update points info
            let points =  makeRequestPromise(['./scripts/findCentaurPoints.php?user=', result].join(''));

            return points
            

	}).then((result) => {

		console.log(result);


            //update points info 
		document.getElementById("total-points").innerHTML = result[0];
		document.getElementById("mapped").innerHTML = result[1];
		document.getElementById("notSure").innerHTML = result[2];
		document.getElementById("rejected").innerHTML = result[3];



		let leaderBoard = result[4];

		let sorted = leaderBoard.sort(compare);
		

		document.getElementById('user-first').innerHTML = sorted[0][0];
		document.getElementById('points-first').innerHTML = sorted[0][1];
		document.getElementById('user-second').innerHTML = sorted[1][0];
		document.getElementById('points-second').innerHTML = sorted[1][1];
		document.getElementById('user-third').innerHTML = sorted[2][0];
		document.getElementById('points-third').innerHTML = sorted[2][1];
		document.getElementById('user-fourth').innerHTML = sorted[3][0];
		document.getElementById('points-fourth').innerHTML = sorted[3][1];
		document.getElementById('user-fifth').innerHTML = sorted[4][0];
		document.getElementById('points-fifth').innerHTML = sorted[4][1];
		

    		makeMap();



	})

    

}


</script>

</html>
